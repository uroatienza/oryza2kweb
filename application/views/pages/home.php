<div class="page-header">
<h1>ORYZA 2K Web</h1>
</div>
<p class="lead">The ORYZA2000 Web Front-end</p>
<div class="container-fluid">
    <div class="hero-unit">
        <h2>ORYZA2000</h2>
        <p>ORYZA2000 is a crop modeling tool used to simulate the growth, development, and water balance of lowland rice under conditions of potential production, and water- and/or nitrogen-limitations. It was calibrated and validated for 18 popular rice varieties in 15 locations throughout Asia.</p>
        <p>Since 2009, ORYZA2000 has been modified from a crop model toward a cropping model aimed mainly at abiotic stress. The modeling group in International Rice Research Institute encourages tests and applications of ORYZA2000 and welcomes feedback. The modeling group also provides minor tech support to users.</p>
    </div>
    <div class="row">
        <div class="span-3" style="width: 60%">
            <h2>News</h2>
            <div>
                <article>
                    <header>
                        <h3>Lorem ipsum dolor sit amet.</h3>
                        <time>2013-08-31</time>
                    </header>
                    <p>The quick brown cat jumped over the lazy vixen. The quick brown cat jumped over the lazy vixen. The quick brown cat jumped [...]</p>
                </article>
                <article>
                    <header>
                        <h3>Lorem ipsum dolor sit amet.</h3>
                        <time>2013-08-31</time>
                    </header>
                    <p>The quick brown cat jumped over the lazy vixen. The quick brown cat jumped over the lazy vixen. The quick brown cat jumped [...]</p>
                </article>
            </div>
            <div>
                <a href="#">Older Posts</a>
                <a href="#" style="float: right">Newer Posts</a>
            </div>
        </div>
        <div class="span-2">
            <h2>Recent simulations</h2>
            <ul>
                <li><a href="<?= base_url() ?>index.php/input#">Simulation 1</a></li>
                <li><a href="<?= base_url() ?>index.php/input#">Simulation 2</a></li>
                <li><a href="<?= base_url() ?>index.php/input#">Simulation 3</a></li>
                <li><a href="<?= base_url() ?>index.php/input#">Simulation 4</a></li>
                <li><a href="<?= base_url() ?>index.php/input#">Simulation 5</a></li>
            </ul>
        </div>
    </div>
</div>
