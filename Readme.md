# Oryza2kWeb

A mission to webify that Oryza2000 crop modeling tool from [IRRI](https://sites.google.com/a/irri.org/oryza2000).

## Involved

Is the [PUP Programming Guild](https://fb.com/puprogrammingguild) that is composed of the ff.

* awkwardusername
* Temoto-kun
* Carl
* Jeremiah

## Purpose

This is for the upcoming [Bigas2Hack](http://rice-hackathon.irri.org/) competition to be held later this year.

Note: Use Notepad++ for opening the fortran files.
